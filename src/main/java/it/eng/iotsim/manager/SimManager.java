package it.eng.iotsim.manager;

import java.util.Map;

import java.util.Set;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iotsim.configuration.*;
import it.eng.iotsim.model.MeasureEntity;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.utils.RestUtils;



public class SimManager{

	private static final Logger LOGGER = Logger.getLogger(SimManager.class.getName() );

	private static int period_inseconds = Integer.parseInt(ConfSimulator.getString("period_inseconds"));
	private static long period = period_inseconds * 1000;
	private static Map<String, Timer> timers = new HashMap<>();

	
	/** Start simulation for single device */
	public static void startTimeTask(String fiwareservice, String fiwareservicepath, String categoryName, String categoryApikey, String deviceid, MeasureEntity measureConf){
		
		String attributeid = measureConf.getId();
		period_inseconds = measureConf.getDelaySeconds();
		period = period_inseconds * 1000;
	
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				LOGGER.log(Level.INFO, "# Run simulation data...");
					runSimulation(fiwareservice, fiwareservicepath, categoryName, categoryApikey, deviceid, measureConf);
			}
		}, 1000, period);
		
		timers.put(fiwareservice+fiwareservicepath+deviceid+attributeid, timer);

	}

	
	/** Checks status of context simulation */
	public static boolean isRunningTimeTask(String fiwareservice, String fiwareservicepath, String deviceid, String attributeid){
		
		
		Timer t = timers.get(fiwareservice+fiwareservicepath+deviceid+attributeid);
		return (t!=null);
		//return false; // FOR TESTING
		
	}
		
	/** Stop device simulation */
	public static void stopTimeTask(String fiwareservice, String fiwareservicepath, String deviceid, String attributeid){
		
		LOGGER.log(Level.INFO, "#Stopping Task...");
		Timer t = timers.remove(fiwareservice+fiwareservicepath+deviceid+attributeid);
	
		if(t!=null){
			t.cancel();
			t.purge();
			t = null;
		}

		System.gc();
	}
	
	public static void stopTimeTask(String fiwareservice, String fiwareservicepath){
		Timer t = timers.remove(fiwareservice+fiwareservicepath);

		if(t!=null){
			t.cancel();
			t.purge();
			t = null;
		}

		System.gc();
	}

	public static boolean isRunning(String fiwareservice, String fiwareservicepath){
		Timer t = timers.get(fiwareservice+fiwareservicepath);
		return (t!=null);
	}


	/** Runs simulation for single device */
	private static void runSimulation(String fiwareservice, String fiwareservicepath, String categoryName, String categoryApikey, String deviceid, MeasureEntity measureConf) {
		
		
		try {
						String orionEndpoint = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v2.entities") ;
						String queryString = "/" + deviceid;
						orionEndpoint = orionEndpoint + queryString;

						fiwareservicepath = ((fiwareservicepath.substring(0, 1).equals( "/")) ? fiwareservicepath.replace("/", "") : fiwareservicepath);
						
						
						Map<String, String> headers = new HashMap<>();
						headers.put("fiware-service", fiwareservice);
						headers.put("fiware-servicepath", "/" + fiwareservicepath);
						String serviceResponse = RestUtils.consumeGet(orionEndpoint,headers);
						
						Set<MeasureEntity> measures = new HashSet<>();
						if (measureConf.getId() != null && !measureConf.getId().equals("") && !measureConf.getId().equals("undefined") ){
							measures = SimUtils.loadBusinessDataModel(categoryName, measureConf);
						}else {
							measures = SimUtils.loadBusinessDataModel(categoryName);
						}
						
						

						// Get Devices list
						Type type = new TypeToken<DeviceEntityBean>(){}.getType();
						DeviceEntityBean device = new Gson().fromJson(serviceResponse, type);
						
						if (!device.getId().contains("Device:")){
							try {
								LOGGER.info("###### DEVICE CONTEXT [" + fiwareservice + "|" + fiwareservicepath + "] ######");
								LOGGER.info("###### DEVICE [" + device.getId() + "] ######");
								String retrieve_data_mode = device.getRetrieveDataMode().getValue();

								if (retrieve_data_mode.equalsIgnoreCase("push")){
									SimUtils.pushMeasure(fiwareservice, fiwareservicepath, categoryApikey, device, measures);
								} else if (retrieve_data_mode.equalsIgnoreCase("pull")){
									SimUtils.pullMeasure(fiwareservice, fiwareservicepath, categoryApikey, device, measures);
								}
							} catch (Exception e){
								LOGGER.severe("=============================");
								LOGGER.severe(e.getMessage());
								e.printStackTrace();
							}
							
						}
						
		}
		catch (Exception e) {
			LOGGER.severe("=============================");
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
		
		
	}
	
	

}