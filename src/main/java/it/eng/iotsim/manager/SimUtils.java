package it.eng.iotsim.manager;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import it.eng.iotsim.configuration.ConfOrionCB;
import it.eng.iotsim.configuration.ConfSimulator;
import it.eng.iotsim.model.MeasureEntity;
import it.eng.iotsim.tools.Idas;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.tools.model.IdasDevice;
import it.eng.iotsim.tools.model.IdasDeviceCommand;
import it.eng.iotsim.utils.IOTAgentManager;
import it.eng.iotsim.utils.RestUtils;

public class SimUtils {

	private static final Logger LOGGER = Logger.getLogger(SimUtils.class.getName() );
	
	
	public static boolean hasDeviceId(String deviceid){
	    boolean out = true;
		try{
			if (deviceid == null || deviceid.trim() == "") {
				out = false;
			}
	    } 
	    catch (Exception exception){
	        out = false;
	    }
		
		return out;
	}
	
	public static boolean hasMeasureId(String measureid){
	    boolean out = true;
		try{
			if (measureid == null || measureid.trim() == "") {
				out = false;
			}
	    } 
	    catch (Exception exception){
	        out = false;
	    }
		
		return out;
	}
	
	
	
public static Set<DeviceEntityBean> getContextAllDevices(String fiwareservice, String fiwareservicepath) {
		
		Set<DeviceEntityBean> devices = new HashSet<DeviceEntityBean>();
		try {
			// /*				
			//"http://217.172.12.177:1026/v2/entities?q=opType!=deleted";
			String orionEndpoint = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v2.entities") ;
			String queryString = "?q=opType!=deleted";
			orionEndpoint = orionEndpoint + queryString;

			Map<String, String> headers = new HashMap<String, String>();
			headers.put("fiware-service", fiwareservice);
			headers.put("fiware-servicepath", "/" + fiwareservicepath);
			String serviceResponse = RestUtils.consumeGet(orionEndpoint,headers);

			// Get Devices list
			Type type = new TypeToken<Set<DeviceEntityBean>>(){}.getType();
			devices = new Gson().fromJson(serviceResponse, type);

			Set<DeviceEntityBean> devicesToInclude = new HashSet<>();
			for(DeviceEntityBean dev:devices){
			//	if (!dev.getId().contains("Device:")){ //TODOFIL
					devicesToInclude.add(dev);
				//}
			}
			devices.retainAll(devicesToInclude);
		} catch (Exception e) {
				LOGGER.severe(e.getMessage());
				e.printStackTrace();
			}
			
		return devices;
	}

public static DeviceEntityBean getOCBDevice(String fiwareservice, String fiwareservicepath, String deviceId) {
	
	DeviceEntityBean device = new DeviceEntityBean();
	try {
		// /*				
		//"http://217.172.12.177:1026/v2/entities?q=opType!=deleted";
		String orionEndpoint = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v2.entities") ;
		String queryString = "/"+deviceId;
		orionEndpoint = orionEndpoint + queryString;

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", "/" + fiwareservicepath);
		String serviceResponse = RestUtils.consumeGet(orionEndpoint,headers);

		// Get Devices list
		Type type = new TypeToken<DeviceEntityBean>(){}.getType();
		device = new Gson().fromJson(serviceResponse, type);


	} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
		
	return device;
}



/** 
 * Load the business urbanservice, and the related attributes to be measured
 * in the configuration_simulator.properties file
 * 
 */
public static Set<MeasureEntity> loadBusinessDataModel(String business) {

	String elem = ConfSimulator.getString(business.toLowerCase());
	LOGGER.info("Simulator - attrib " + elem);
	String[] attributesToMeasure = elem.split(",");

	Set<MeasureEntity> measures = new HashSet<MeasureEntity>();

	try {
		
		
		for (String attr :attributesToMeasure){
			MeasureEntity measure = new MeasureEntity();

			String id = attr.substring(0, attr.indexOf("|"));
			measure.setId(id);
			attr = attr.replace(id+"|", "");

			String type = attr.substring(0, attr.indexOf("|"));
			measure.setType(type);
			attr = attr.replace(type+"|", "");

			String valueRange =	attr.substring(1, attr.length()-1);	// remove []
			measure.setValueRange(valueRange);
			measure = assignValueToMisure (  measure,  type);
			
			measures.add(measure);
		}
	}catch (Exception e) {
		e.printStackTrace();
		LOGGER.severe("NO ATTRIBUTE HAS BEEN CONFIGURED INTO THE SIMULATOR PROPERTIES FILE");
	}
	return measures;

}

/** 
 * Load the business urbanservice, and the related attributes to be measured
 * in the configuration_simulator.properties file
 * 
 */
public static MeasureEntity loadBusinessDataModelD(String urbanserviceName, MeasureEntity simulatedMeasure ) {

	String measure_id = simulatedMeasure.getId();
	String measure_type = simulatedMeasure.getType();
	
	String elem = ConfSimulator.getString(urbanserviceName.toLowerCase());
	String unitOfMeasure = ConfSimulator.getString("unit_of_measure");
	int radiusDistance = Integer.parseInt(ConfSimulator.getString("radius_distance"));
	
	String[] attributesToMeasure = elem.split(",");

	MeasureEntity measure = new MeasureEntity();
	measure.setRadiusDistance(radiusDistance);
	measure.setUnitOfMeasure(unitOfMeasure);
	measure.setMapcenterLat(simulatedMeasure.getMapcenterLat());
	measure.setMapcenterLng(simulatedMeasure.getMapcenterLng());
	
	try {
		
		boolean found = false;
		
		for (String attr :attributesToMeasure){
			
			String id = attr.substring(0, attr.indexOf("|"));
			
			if (measure_id.equalsIgnoreCase(id)) {
				
				measure.setId(id);
				attr = attr.replace(id+"|", "");

				String type = attr.substring(0, attr.indexOf("|"));
				measure.setType(type);
				attr = attr.replace(type+"|", "");

				String valueRange =	attr.substring(1, attr.length()-1);	// remove []
				measure.setValueRange(valueRange);

				measure = assignValueToMisure (  measure,  measure_type);
				found = true;
				break;
			
			}
			
			//if in the properties there isn't a configuration by measure_id, use the one by type
			if (!found) {
				
				 elem = ConfSimulator.getString(measure_type.toLowerCase());
				 
				 	measure.setId(measure_id);
					measure.setType(measure_type);
					String valueRange =	elem.substring(1, elem.length()-1);	// remove []
					measure.setValueRange(valueRange);
					measure = assignValueToMisure (  measure,  measure_type);
			}
		}
	}catch (Exception e) {
		e.printStackTrace();
		LOGGER.severe("NO ATTRIBUTE HAS BEEN CONFIGURED INTO THE SIMULATOR PROPERTIES FILE");
	}
	return measure;

}


private static MeasureEntity assignValueToMisure ( MeasureEntity measure, String measure_type) {
	
	String[] values = measure.getValueRange().split("-");
	//create value
	if ( measure_type.equalsIgnoreCase("integer") || measure_type.equalsIgnoreCase("number") || measure_type.equalsIgnoreCase("int") ) {
		int min = Integer.parseInt(values[0]);
		int max = Integer.parseInt(values[1]);
		int val = randInt(min, max);
		measure.setValue(val);
	}else if(measure_type.equalsIgnoreCase("binary")) {
		int min = Integer.parseInt(values[0]);
		int max = Integer.parseInt(values[1]);
		int val = randInt(min, max);
		measure.setValue(Integer.toBinaryString(val));
	}else if(measure_type.equalsIgnoreCase("hex")) {
		int min = Integer.parseInt(values[0]);
		int max = Integer.parseInt(values[1]);
		int val = randInt(min, max);
		measure.setValue(Integer.toHexString(val));
	}else if (measure_type.equalsIgnoreCase("float") || measure_type.equalsIgnoreCase("decimal")){
		float min = Float.parseFloat(values[0]);
		float max = Float.parseFloat(values[1]);
		float val = randFloat(min, max);
		measure.setValue(val);
	
	}else if (measure_type.equalsIgnoreCase("double") ){
		double min = Double.parseDouble(values[0]);
		double max = Double.parseDouble(values[1]);
		double val = randDouble(min, max);
		measure.setValue(val);	
		
	} else if (measure_type.equalsIgnoreCase("string") || measure_type.equalsIgnoreCase("text") || measure_type.equalsIgnoreCase("chars")){
		int i = randInt(0, values.length-1); 
		String val = values[i];
		measure.setValue(val);
		
	} else if (measure_type.equalsIgnoreCase("DateTime") || measure_type.equalsIgnoreCase("date") || measure_type.equalsIgnoreCase("timestamp") ){
		
		 //2019-10-10T13:05:08.090Z
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    String strDate = sdfDate.format(System.currentTimeMillis());
		measure.setValue(strDate);	
	
	}else if (measure_type.equalsIgnoreCase("boolean")  ){
			
			boolean boolRnd = randBoolean();
			measure.setValue(boolRnd);	
	}else if (measure_type.equalsIgnoreCase("geo:point")  ){
		
		double[] geoRnd = randGeo(measure.getMapcenterLat(),measure.getMapcenterLng(), measure.getRadiusDistance(), measure.getUnitOfMeasure());
		
		String geoRndS = geoRnd[0]+","+geoRnd[1];
		
		
		
		
		measure.setValue(geoRndS);			
	}
	
	return measure;
}



public static Set<MeasureEntity> loadBusinessDataModel(String business, MeasureEntity measureConfig) {

	LOGGER.info("Simulator - CONFIG " + measureConfig.getId());
	
	Set<MeasureEntity> measures = new HashSet<MeasureEntity>();
	try {
			measureConfig = assignValueToMisure(measureConfig,measureConfig.getType() );
			measures.add(measureConfig);
		}
	catch (Exception e) {
		e.printStackTrace();
		LOGGER.severe("NO ATTRIBUTE HAS BEEN CONFIGURED INTO THE SIMULATOR PROPERTIES FILE");
	}
	return measures;
}


/** Simulates the device that push a measure */
public static void pushMeasure(String fiwareservice, String fiwareservicepath, String apikey, DeviceEntityBean dev, Set<MeasureEntity> measures) {

	String deviceid = dev.getId();
	LOGGER.info("# DEVICE ID [" + deviceid + "] SENDS MEASURE");
	String retrieve_data_mode = dev.getRetrieveDataMode().getValue();

	Map<String, String> headers = new HashMap<String, String>();
	headers.put("fiware-service", fiwareservice);
	headers.put("fiware-servicepath", "/" + fiwareservicepath);
	
	String dataformat_protocol = dev.getDataformatProtocol().getValue();
	String transport_protocol = dev.getTransportProtocol().getValue();
	LOGGER.info("# CASE [" + retrieve_data_mode + "] #" + " format " + dataformat_protocol + " over " + transport_protocol);
	
	switch(transport_protocol) {
		case "MQTT":
			MQTTSim mqttsimulator = new MQTTSim(deviceid, apikey, dev, measures);		
			mqttsimulator.simulatePush(dataformat_protocol);
			break;
		case "HTTP":
			HTTPSim httpsimulator = new HTTPSim(fiwareservice, fiwareservicepath, deviceid, apikey, dev, measures);		
			httpsimulator.simulatePush(dataformat_protocol);
			break;
		case "SIGFOX":
			DeviceSimulator sigfoxsimulator = new DeviceSimulator(fiwareservice, fiwareservicepath, deviceid, apikey, dev, measures);
			sigfoxsimulator.simulatePush(dataformat_protocol);
			break;
		case "COAP":
			DeviceSimulator lwm2msimulator = new DeviceSimulator(fiwareservice, fiwareservicepath, deviceid, apikey, dev, measures);
			lwm2msimulator.simulatePush(dataformat_protocol);
			break;
		case "LORA":
			DeviceSimulator lorasimulator = new DeviceSimulator(fiwareservice, fiwareservicepath, deviceid, apikey, dev, measures);
			lorasimulator.simulatePush(dataformat_protocol);
			break;
	}

}



/** Simulates the device that receive a command and send the measure */
public static void pullMeasure(String fiwareservice, String fiwareservicepath, String apikey, DeviceEntityBean dev, Set<MeasureEntity> measures) throws Exception {

	String deviceId = dev.getId();
	String retrieve_data_mode = dev.getRetrieveDataMode().getValue();
	
	String dataformat_protocol = dev.getDataformatProtocol().getValue();
	String transport_protocol = dev.getTransportProtocol().getValue();
	LOGGER.info("# CASE [" + retrieve_data_mode + "] #" + " format " + dataformat_protocol + " over " + transport_protocol);
	LOGGER.info("### Platform (OrionCB) sends command to device " + deviceId);
	
	// Get Configured device command on idas
	String commandName = "";
	String deviceEndpoint = "";

	Idas idas = IOTAgentManager.switchIOTA(dev.getDataformatProtocol().getValue());

	IdasDevice idasDevice = idas.getDevice(fiwareservice, fiwareservicepath, deviceId);
	
	Set<IdasDeviceCommand> cmds = idasDevice.getCommands();
	for (IdasDeviceCommand cmd :cmds){
		if (cmd.getName().toLowerCase().contains(ConfSimulator.getString("device.sendmeasure.command").toLowerCase())){
			commandName = cmd.getName();
		} 
	}
	
	if (!commandName.isEmpty()){
		deviceEndpoint = idasDevice.getEndpoint();
		LOGGER.info("# ENDPOINT: " + deviceEndpoint);

		switch(transport_protocol) {
		case "MQTT":
			MQTTSim mqttsimulator = new MQTTSim(fiwareservice, fiwareservicepath, deviceId, apikey, dev, measures);
			mqttsimulator.simulatePull(dataformat_protocol, commandName);
			break;
		case "HTTP":
			HTTPSim httpsimulator = new HTTPSim(fiwareservice, fiwareservicepath, deviceId, apikey, dev, measures);
			httpsimulator.simulateOrionPull(dataformat_protocol, commandName);	
			break;
		case "SIGFOX":
			DeviceSimulator sigfoxsimulator = new DeviceSimulator(fiwareservice, fiwareservicepath, deviceId, apikey, dev, measures);
			sigfoxsimulator.simulateOrionPull(dataformat_protocol, commandName);
			break;
		case "COAP":
			DeviceSimulator lwm2msimulator = new DeviceSimulator(fiwareservice, fiwareservicepath, deviceId, apikey, dev, measures);
			lwm2msimulator.simulateOrionPull(dataformat_protocol, commandName);
			break;
		case "LORA":
			DeviceSimulator lorasimulator = new DeviceSimulator(fiwareservice, fiwareservicepath, deviceId, apikey, dev, measures);
			lorasimulator.simulateOrionPull(dataformat_protocol, commandName);
			break;			
		}
	} else {
		LOGGER.info("# Send measure command is not configured for device " + deviceId);
	}

	

}




/**
 * Returns a psuedo-random int number between min and max, inclusive.
 */
public static int randInt(int min, int max) {
	Random rand = new Random();
	int randomNum = rand.nextInt((max - min) + 1) + min;
	return randomNum;
}
/**
 * Returns a psuedo-random float number between min and max, inclusive.
 */
public static float randFloat(float min, float max) {
	Random rand = new Random();
	float result = rand.nextFloat() * (max - min) + min;
	return result;
}

/**
 * Returns a psuedo-random double number between min and max, inclusive.
 */
public static double randDouble(double min, double max) {
	Random rand = new Random();
	double result = rand.nextDouble() * (max - min) + min;
	return result;
}

/**
 * Returns a psuedo-random boolean
 */
public static boolean randBoolean() {
	Random rand = new Random();
	boolean result = rand.nextBoolean();
	return result;
}

/**
 * Returns a psuedo-random geo point
 */
public static double[] randGeo(double centerX, double centerY, int radius, String unitOfMeasure) {
	
	
	double[] results = new double[] {centerX,centerY };
	
	
	double R = radius;
	double paramConvKm = 6371;
	double paramConvMi = 3959;
	
	//https://stackoverflow.com/questions/12180290/convert-kilometers-to-radians
	if (("km").equalsIgnoreCase(unitOfMeasure))
		R = radius/paramConvKm; 
	else if (("mi").equalsIgnoreCase(unitOfMeasure))
		R = radius/paramConvMi;
	
	//https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly/50746409#50746409	
	double rnd = randDouble(0,1);
	
	double r = R * Math.sqrt(rnd);
	
	double theta = rnd * 2 * Math.PI;
	
	
	double x = centerX + r * Math.cos(theta);
	double y = centerY + r * Math.sin(theta);
	
	results[0]=x;
	results[1]=y;
	
	return results;
}

/** 
 * Load the business urbanservice, and the related attributes to be measured
 * in the configuration_simulator.properties file
 * 
 */
public static Set<MeasureEntity> loadBusinessDataModel(String urbanserviceName, String measure_id , String measure_type, String fiwareService) {

	String elem = ConfSimulator.getString(urbanserviceName.toLowerCase());
	String unitOfMeasure = ConfSimulator.getString("unit_of_measure");
	int radiusDistance = Integer.parseInt(ConfSimulator.getString("radius_distance"));
	
	String[] attributesToMeasure = elem.split(",");

	Set<MeasureEntity> measures = new HashSet<MeasureEntity>();
	
	boolean found = false;
	
	try {
		
		for (String attr :attributesToMeasure){

			String id = attr.substring(0, attr.indexOf("|"));
			
			if (measure_id.equalsIgnoreCase(id)) {
				
				MeasureEntity measure = new MeasureEntity();

				measure.setId(id);
				attr = attr.replace(id+"|", "");

				String type = attr.substring(0, attr.indexOf("|"));
				measure.setType(type);
				attr = attr.replace(type+"|", "");

				String valueRange =	attr.substring(1, attr.length()-1);	// remove []
				measure.setValueRange(valueRange);

			
				measure = assignValueToMisure (  measure,  measure_type);
				measures.add(measure);
				found = true;
				break;
			
			}
		}	
		
	}catch (Exception e) {
		
		LOGGER.log(Level.SEVERE,"NO ATTRIBUTE HAS BEEN CONFIGURED INTO THE SIMULATOR PROPERTIES FILE "+ e.getMessage());
	}
	
	try {
	
		//if in the properties there isn't a configuration by measure_id, use the one by type
		if (!found) {
			
			 elem = ConfSimulator.getString(measure_type.toLowerCase());
			 
			 MeasureEntity measure = new MeasureEntity();
			 	measure.setId(measure_id);
				measure.setType(measure_type);
				String valueRange =	elem.substring(1, elem.length()-1);	// remove []
				measure.setValueRange(valueRange);
				measure.setRadiusDistance(radiusDistance);
				measure.setUnitOfMeasure(unitOfMeasure);
				measure = assignValueToMisure (  measure,  measure_type);
				measures.add(measure);
		}
	
	}catch (Exception e) {
		LOGGER.log(Level.SEVERE, "NO ATTRIBUTE HAS BEEN CONFIGURED INTO THE SIMULATOR PROPERTIES FILE "+ e.getMessage());
	}
	
	return measures;

}
	
}
