package it.eng.iotsim.manager;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import it.eng.iotsim.configuration.ConfIdas;
import it.eng.iotsim.configuration.ConfIotaJSON;
import it.eng.iotsim.configuration.ConfIotaUL20;
import it.eng.iotsim.model.MeasureEntity;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.utils.RestUtils;

public class HTTPSim extends DeviceSimulator {

	private static final Logger LOGGER = Logger.getLogger(HTTPSim.class.getName());

	public HTTPSim(String fiwareservice, String fiwareservicepath, String deviceid, String apikey, DeviceEntityBean dev,
			Set<MeasureEntity> measures) {
		super(fiwareservice, fiwareservicepath, deviceid, apikey, dev, measures);
	}

	@Override
	public void simulateUL() {
		
		Map<String, String> headers = new HashMap<>();
		headers.put("fiware-service", this.getFiwareservice());
		headers.put("fiware-servicepath", "/" + this.getFiwareservicepath());
		
		try{
			//String idasEndpoint = "http://217.172.12.177:8086/iot/d?i=" + deviceId + "&k=" + apikey; 
			String idasEndpoint = "http://" + ConfIotaUL20.getString("iota.ul20.host")+":"+  ConfIotaUL20.getString("iota.ul20.southbound.port") + ConfIdas.getString("iota.resource");
			String queryStr= "?i=" + this.getDev().getId() + "&k=" + this.getApikey();
			idasEndpoint = idasEndpoint + queryStr;
			LOGGER.info("# IDAS ENDPOINT: " + idasEndpoint);

			String payload = "";
			for (MeasureEntity mes:this.getMeasures()){
				String _payload = mes.getId()+"|"+mes.getValue();
				payload = _payload ;
				_payload = "";
				try{
					LOGGER.info("# DEVICE ID [" + this.getDeviceid() + "] IS SENDING MEASURE...");
					RestUtils.consumePost(idasEndpoint, payload, MediaType.TEXT_PLAIN_TYPE, headers);
					LOGGER.info("# Done!");
				}catch (Exception e) {
					LOGGER.info("Attribute " + mes.getId() + " not configured" );
					LOGGER.severe(e.getMessage());
				}
			}
		}catch (Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
	}

	@Override
	public void simulateJSON() {
		
		Map<String, String> headers = new HashMap<>();
		headers.put("fiware-service", this.getFiwareservice());
		headers.put("fiware-servicepath", "/" + this.getFiwareservicepath());

		try{
			//Logger.getLogger(Simulator.class).info("=== DEVICE " + deviceId + " IS SENDING MEASURE ===");
			
			String idasEndpoint = "http://" + ConfIotaJSON.getString("iota.json.host.south")+":"+  ConfIotaJSON.getString("iota.json.southbound.port") + ConfIdas.getString("iota.resource");
			
			String queryStr= "?i=" + this.getDev().getId() + "&k=" + this.getApikey();
			idasEndpoint = idasEndpoint + queryStr;
			LOGGER.info("# IDAS ENDPOINT: " + idasEndpoint);

			String payload = "";
			for (MeasureEntity mes:this.getMeasures()){
				//Logger.getLogger(Simulator.class).info("id|type|range|value >> " + mes.getId()+"|"+mes.getType()+"|"+mes.getValueRange()+"|"+mes.getValue());
				String _payload = "{\"" + mes.getId()+"\": \""+mes.getValue() + "\"";
				payload = _payload + "}";
				_payload = "";
				LOGGER.info("# PAYLOAD: " + payload);
				try {
					LOGGER.info("# DEVICE ID [" + this.getDeviceid() + "] IS SENDING MEASURE...");
					RestUtils.consumePost(idasEndpoint, payload, MediaType.APPLICATION_JSON_TYPE, headers);
					LOGGER.info("# Done!");
				}catch (Exception e) {
					LOGGER.info("Attribute " + mes.getId() + " not configured" );
					LOGGER.severe(e.getMessage());
				}
			}
		}catch (Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
		
	}
	
//	@Override
//	public void simulatePull(String dataformat_protocol, String commandName) {
//		
//		Map<String, String> headers = new HashMap<String, String>();
//		headers.put("fiware-service", this.getFiwareservice());
//		headers.put("fiware-servicepath", "/" + this.getFiwareservicepath());
//		
//		String commandMessage = "sendmeasure";
//		
//		try {
//			String orionEndpointV1 = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v1.updateContext") ;
//			String payload = "{\"contextElements\": [ {\"type\": \"Device\","+
//					"\"isPattern\": \"false\", " +
//					" \"id\": \"" + this.getDeviceid() + "\","+
//					"\"attributes\": [ {"  +
//					"\"name\": \"" + commandName + "\"," +
//					"\"type\": \"command\"," + 
//					// deviceid:annapull02|apikey:GmJszisVlK83sGR|dataformatprotocol:UL2_0
//					"\"value\": \"" +
//					"deviceid:" + this.getDeviceid() + 
//					"|apikey:" + this.getApikey() + 
//					"|dataformatprotocol:" + dataformat_protocol + 
//					"|servicepath:" + this.getFiwareservicepath() + 
//					"|cmdmessage:" +
//					commandMessage +
//					"\"" +
//					"}]"	+
//					"}]," + 
//					"\"updateAction\": \"UPDATE\"" +
//					"}" ;
//			LOGGER.info("# Platform is sending command to device...");
//			RestUtils.consumePost(orionEndpointV1, payload, MediaType.APPLICATION_JSON_TYPE, headers);
//			LOGGER.info("# Command successully sent to device id [" + this.getDeviceid() + "]");
//		}catch (Exception e) {
//			LOGGER.severe(e.getMessage());
//			LOGGER.log(Level.INFO, "# Device id [" + this.getDeviceid() + "] not available");
//		}
//	}

}
