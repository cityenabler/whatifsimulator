package it.eng.iotsim.manager;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import it.eng.iotsim.configuration.ConfMQTTBroker;
import it.eng.iotsim.configuration.ConfMQTTSimulator;
import it.eng.iotsim.configuration.ConfOrionCB;
import it.eng.iotsim.model.MeasureEntity;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.utils.MqttUtils;
import it.eng.iotsim.utils.RestUtils;

public class MQTTSim extends DeviceSimulator {

	private static final Logger LOGGER = Logger.getLogger(MQTTSim.class.getName());
	
	public MQTTSim(String deviceid, String apikey, DeviceEntityBean dev, Set<MeasureEntity> measures) {
		super(deviceid, apikey, dev, measures);
	}
	
	public MQTTSim(String fiwareservice, String fiwareservicepath, String deviceid, String apikey, DeviceEntityBean dev,
			Set<MeasureEntity> measures) {
		super(fiwareservice, fiwareservicepath, deviceid, apikey, dev, measures);
	}

	@Override
	public void simulateUL() {
		
		boolean mqttBrokerSslEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.enabled"));
		String mqttBrokerProtocol = mqttBrokerSslEnabled ? 
				ConfMQTTBroker.getString("mqtt.broker.protocol.ssl") : ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");	
		String MQTTBrokerHost = ConfMQTTBroker.getString("mqtt.broker.ul20.private.host");
		String MQTTBrokerUL20Port = ConfMQTTBroker.getString("mqtt.broker.ul20.private.port");
		boolean  MQTTBrokerUL20AuthEnabled = Boolean.valueOf(ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.enabled"));
		String  MQTTBrokerUL20AuthUsername = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.username");
		String  MQTTBrokerUL20AuthPassword = ConfMQTTBroker.getString("mqtt.broker.ul20.authentication.password");
		String MQTTBrokerUL20 = mqttBrokerProtocol + MQTTBrokerHost + ":" + MQTTBrokerUL20Port ;
		String mqttBrokerSslCaFilePath = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.caFilePath");
		String mqttBrokerSslClientCrtFilePath = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.clientCrtFilePath");
		String mqttBrokerSslClientKeyFilePath = ConfMQTTBroker.getString("mqtt.broker.ul20.ssl.clientKeyFilePath");
		
		try{
			LOGGER.info("# MQTTBroker configuration: " + MQTTBrokerUL20);
			String payload = "";
			for (MeasureEntity mes:this.getMeasures()){
				String _payload = mes.getId()+"|"+mes.getValue();
				payload = _payload ;
				_payload = "";
				LOGGER.info("# Measure: " + payload);
				try{
					// PUBLISH MEASURE ON MOSQUITTO BROKER
					String broker =  MQTTBrokerUL20;
					String topic = "/" + this.getApikey() + "/" + this.getDeviceid() + "/attrs"; ///GmJszisVlK83sGR/mqttpushul20/attrs
					String content = payload; 	//"status|occupied";
					LOGGER.info("# topic: " + topic);
					LOGGER.info("# DEVICE ID [" + this.getDeviceid() + "] IS Publishing MEASURE...");
					String resultCode = MqttUtils.mqttPublish(broker, content, topic, MQTTBrokerUL20AuthEnabled, MQTTBrokerUL20AuthUsername, MQTTBrokerUL20AuthPassword,
							mqttBrokerSslEnabled, mqttBrokerSslCaFilePath, mqttBrokerSslClientCrtFilePath, mqttBrokerSslClientKeyFilePath);
					
					LOGGER.info("# Done!");
					LOGGER.info("Simulator - mqtt result: " + resultCode);
				}catch (Exception e) {
					LOGGER.info("Attribute " + mes.getId() + " not configured" );
					LOGGER.severe(e.getMessage());
				}
			}
		} catch(Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
		
	}

	@Override
	public void simulateJSON() {

		boolean mqttBrokerSslEnabled = Boolean.parseBoolean(ConfMQTTBroker.getString("mqtt.broker.json.ssl.enabled"));	
		String mqttBrokerProtocol = mqttBrokerSslEnabled ? 
				ConfMQTTBroker.getString("mqtt.broker.protocol.ssl") : ConfMQTTBroker.getString("mqtt.broker.protocol.tcp");
		String MQTTBrokerHost = ConfMQTTBroker.getString("mqtt.broker.json.private.host");
		String MQTTBrokerJSONPort = ConfMQTTBroker.getString("mqtt.broker.json.private.port");
		boolean  MQTTBrokerJSONAuthEnabled = Boolean.parseBoolean(ConfMQTTBroker.getString("mqtt.broker.json.authentication.enabled"));
		String  MQTTBrokerJSONAuthUsername = ConfMQTTBroker.getString("mqtt.broker.json.authentication.username");
		String  MQTTBrokerJSONAuthPassword = ConfMQTTBroker.getString("mqtt.broker.json.authentication.password");
		String MQTTBrokerJSON = mqttBrokerProtocol + MQTTBrokerHost + ":" + MQTTBrokerJSONPort ;
		LOGGER.info("# MQTTBroker configuration: " + MQTTBrokerJSON);
		String mqttBrokerSslCaFilePath = ConfMQTTBroker.getString("mqtt.broker.json.ssl.caFilePath");
		String mqttBrokerSslClientCrtFilePath = ConfMQTTBroker.getString("mqtt.broker.json.ssl.clientCrtFilePath");
		String mqttBrokerSslClientKeyFilePath = ConfMQTTBroker.getString("mqtt.broker.json.ssl.clientKeyFilePath");
		
		try {
			String payload = "";
			for (MeasureEntity mes:this.getMeasures()){
				String _payload = "{\"" + mes.getId()+"\": \""+mes.getValue() + "\"";
				payload = _payload + "}";
				_payload = "";
				LOGGER.info("# Measure: " + payload);
				try{
					// PUBLISH MEASURE ON MOSQUITTO BROKER
					String broker =  MQTTBrokerJSON;
					String topic = "/" + this.getApikey() + "/" + this.getDeviceid() + "/attrs";
					String content = payload; 
					LOGGER.info("# topic: " + topic);
					LOGGER.info("# DEVICE ID [" + this.getDeviceid() + "] IS Publishing MEASURE...");
					String resultCode = MqttUtils.mqttPublish(broker, content, topic, MQTTBrokerJSONAuthEnabled, MQTTBrokerJSONAuthUsername, MQTTBrokerJSONAuthPassword,
							mqttBrokerSslEnabled, mqttBrokerSslCaFilePath, mqttBrokerSslClientCrtFilePath, mqttBrokerSslClientKeyFilePath);
					LOGGER.info("# Done!");
					LOGGER.info("Simulator - mqtt result: " + resultCode);
				}catch (Exception e) {
					LOGGER.info("Attribute " + mes.getId() + " not configured" );
					LOGGER.severe(e.getMessage());
				}
			}
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
		}
	}
	
	@Override
	public void simulatePull(String dataformat_protocol, String commandName) {
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", this.getFiwareservice());
		headers.put("fiware-servicepath", "/" + this.getFiwareservicepath());
		
		String commandMessage = "sendmeasure";
		
		try {
			String mqttDeviceSimEndpoint = ConfMQTTSimulator.getString("mqtt.simulator.protocol") + 
					ConfMQTTSimulator.getString("mqtt.simulator.host") +
					":" + ConfMQTTSimulator.getString("mqtt.simulator.port") +
					ConfMQTTSimulator.getString("mqtt.simulator.resource");
			//LOGGER.info("# MQTT device simulator endpoint: " + mqttDeviceSimEndpoint);
			String payloadDevice =  
					"deviceId:" + this.getDeviceid() + 
					"|apikey:" + this.getApikey() + 
					"|dataformatprotocol:" + dataformat_protocol + 
					"|cmdmessage:" +
					commandMessage;
			try{
				//LOGGER.info("### Sending identity request to MQTTDevice Simulator...");
				RestUtils.consumePost(mqttDeviceSimEndpoint, payloadDevice, MediaType.TEXT_PLAIN_TYPE, headers);
				//LOGGER.info("# Success");
			}catch (Exception e) {
				if (ConfMQTTSimulator.getString("mqtt.simulator.isRunning").equalsIgnoreCase("true")){
					LOGGER.info("### Sending identity request to MQTTDevice Simulator...");
					LOGGER.log(Level.SEVERE, e.getMessage());
					LOGGER.info("### CAUSE: ");
					LOGGER.info("# 1 # MQTTDevice Simulator is NOT up and running");
					LOGGER.info("# 2 # Device " + this.getDeviceid() + " is NOT identified to the MQTTDevice Simulator.");
				}
			}
			/* Platform Send command to device ORION - IDAS - DEVICE */
			String orionEndpointV1 = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v1.updateContext") ;
			LOGGER.info("# endpoint: " + orionEndpointV1);
			String payload = "{\"contextElements\": [ {\"type\": \"Device\","+
					"\"isPattern\": \"false\", " +
					" \"id\": \"" + this.getDeviceid() + "\","+
					"\"attributes\": [ {"  +
					"\"name\": \"" + commandName + "\"," +
					"\"type\": \"command\"," + 
					// deviceid:mqtt-pulljson|apikey:GmJszisVlK83sGR|dataformatprotocol:JSON|servicepath:malaga_1_parking|cmdmessage:message"
					"\"value\": \"" +
					"deviceid:" + this.getDeviceid() + 
					"|apikey:" + this.getApikey() + 
					"|dataformatprotocol:" + dataformat_protocol + 
					"|servicepath:" + this.getFiwareservicepath() + 
					"|cmdmessage:" + 
					commandMessage +
					"\"" +
					"}]"	+
					"}]," + 
					"\"updateAction\": \"UPDATE\"" +
					"}" ;
			LOGGER.info("# Platform is sending command to device...");
			RestUtils.consumePost(orionEndpointV1, payload, MediaType.APPLICATION_JSON_TYPE, headers);
			LOGGER.info("# Command successully sent to device id [" + this.getDeviceid() + "]");
		}catch (Exception e) {
			LOGGER.severe(e.getMessage());
			LOGGER.log(Level.INFO, "# Device id [" + this.getDeviceid() + "] not available");
		}
	}
	
}
