package it.eng.iotsim.manager;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import it.eng.iotsim.configuration.ConfOrionCB;
import it.eng.iotsim.model.DataformatProtocol;
import it.eng.iotsim.model.MeasureEntity;
import it.eng.iotsim.tools.Orion;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.utils.RestUtils;

public class DeviceSimulator {
		
	private String fiwareservice;
	private String fiwareservicepath;
	private String deviceid;
	private String apikey;
	private DeviceEntityBean dev;
	private Set<MeasureEntity> measures;
	
	private static final Logger LOGGER = Logger.getLogger(DeviceSimulator.class.getName());

	public DeviceSimulator(String deviceid, String apikey,
			DeviceEntityBean dev, Set<MeasureEntity> measures) {
		super();
		this.deviceid = deviceid;
		this.apikey = apikey;
		this.dev = dev;
		this.measures = measures;
	}
	
	public DeviceSimulator(String fiwareservice, String fiwareservicepath, String deviceid, String apikey,
			DeviceEntityBean dev, Set<MeasureEntity> measures) {
		super();
		this.fiwareservice = fiwareservice;
		this.fiwareservicepath = fiwareservicepath;
		this.deviceid = deviceid;
		this.apikey = apikey;
		this.dev = dev;
		this.measures = measures;
	}

	public void simulatePush(String dataformat_protocol) {
		if(dataformat_protocol.equalsIgnoreCase(DataformatProtocol.UL2_0.toString())) {
			simulateUL();
		} else if(dataformat_protocol.equalsIgnoreCase(DataformatProtocol.JSON.toString())) {
			simulateJSON();
		} else if(
			dataformat_protocol.equalsIgnoreCase(DataformatProtocol.SIGFOX.toString()) || 
			dataformat_protocol.equalsIgnoreCase(DataformatProtocol.LWM2M.toString()) ||
			dataformat_protocol.equalsIgnoreCase(DataformatProtocol.CAYENNELPP.toString())) 
		{
			simulateOrionPush();
		}
	}
	
	public void simulatePull(String dataformat_protocol, String commandName) {};
	
	public void simulateUL() {};
	
	public void simulateJSON() {};
	
	public void simulateOrionPush() {
		
		String deviceid = this.getDeviceid();
		String orionEndpoint = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v2.entities") ;

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", this.getFiwareservice());
		headers.put("fiware-servicepath", "/" + this.getFiwareservicepath());
		
		try {	
			String payload = "";
			for (MeasureEntity mes:this.getMeasures()){
				String queryString = "/" + deviceid + "/attrs";
				if(this.getDev().getType() != null) {
					queryString = queryString + "?type=" + this.getDev().getType();
				}
				orionEndpoint = orionEndpoint + queryString;
				
				String dateModified = Orion.dateFormat(GregorianCalendar.getInstance().getTime());
				payload = "{\"" + mes.getId() + "\":{\"value\":\"" + mes.getValue() + "\",\"type\":\"" 
				+ this.getDev().getType() + "\"}, \"dateModified\":{\"type\":\"text\", \"value\":\"" 
						+ dateModified + "\"}}";
				
				LOGGER.info("# PAYLOAD: " + mes.getValue());
				try {
					LOGGER.info("# DEVICE ID [" + this.getDeviceid() + "] IS SENDING MEASURE...");
					RestUtils.consumePost(orionEndpoint, payload, MediaType.APPLICATION_JSON_TYPE, headers);
					LOGGER.info("# Done!");
				}catch (Exception e) {
					LOGGER.info("Attribute " + mes.getId() + " not configured" );
					LOGGER.severe(e.getMessage());
				}
			}				
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	};
	
	public void simulateOrionPull(String dataformat_protocol, String commandName) {
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", this.getFiwareservice());
		headers.put("fiware-servicepath", "/" + this.getFiwareservicepath());
		
		String commandMessage = "sendmeasure";
		
		try {
			String orionEndpointV1 = ConfOrionCB.getString("orion.protocol") + ConfOrionCB.getString("orion.host") + ConfOrionCB.getString("orion.v1.updateContext") ;
			String payload = "{\"contextElements\": [ {\"type\": \"Device\","+
					"\"isPattern\": \"false\", " +
					" \"id\": \"" + this.getDeviceid() + "\","+
					"\"attributes\": [ {"  +
					"\"name\": \"" + commandName + "\"," +
					"\"type\": \"command\"," + 
					// deviceid:annapull02|apikey:GmJszisVlK83sGR|dataformatprotocol:UL2_0
					"\"value\": \"" +
					"deviceid:" + this.getDeviceid() + 
					"|apikey:" + this.getApikey() + 
					"|dataformatprotocol:" + dataformat_protocol + 
					"|servicepath:" + this.getFiwareservicepath() + 
					"|cmdmessage:" +
					commandMessage +
					"\"" +
					"}]"	+
					"}]," + 
					"\"updateAction\": \"UPDATE\"" +
					"}" ;
			LOGGER.info("# Platform is sending command to device...");
			RestUtils.consumePost(orionEndpointV1, payload, MediaType.APPLICATION_JSON_TYPE, headers);
			LOGGER.info("# Command successully sent to device id [" + this.getDeviceid() + "]");
		}catch (Exception e) {
			LOGGER.severe(e.getMessage());
			LOGGER.log(Level.INFO, "# Device id [" + this.getDeviceid() + "] not available");
		}
		
	}

	public String getFiwareservice() {
		return fiwareservice;
	}

	public void setFiwareservice(String fiwareservice) {
		this.fiwareservice = fiwareservice;
	}

	public String getFiwareservicepath() {
		return fiwareservicepath;
	}

	public void setFiwareservicepath(String fiwareservicepath) {
		this.fiwareservicepath = fiwareservicepath;
	}

	public String getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public DeviceEntityBean getDev() {
		return dev;
	}

	public void setDev(DeviceEntityBean dev) {
		this.dev = dev;
	}

	public Set<MeasureEntity> getMeasures() {
		return measures;
	}

	public void setMeasures(Set<MeasureEntity> measures) {
		this.measures = measures;
	}

	
}
