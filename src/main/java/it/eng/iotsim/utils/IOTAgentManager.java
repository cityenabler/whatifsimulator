package it.eng.iotsim.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.eng.iotsim.tools.IOTA;
import it.eng.iotsim.tools.IOTA_JSON;
import it.eng.iotsim.tools.IOTA_LORA;
import it.eng.iotsim.tools.IOTA_LWM2M;
import it.eng.iotsim.tools.IOTA_SIGFOX;
import it.eng.iotsim.tools.IOTA_UL;
import it.eng.iotsim.tools.Idas;


public class IOTAgentManager {
	
	private static final Logger LOGGER = Logger.getLogger(IOTAgentManager.class.getName() );
	
	public static Idas switchIOTA(String dataformatProtocol) throws Exception {
		Idas idas = null;
		try{ 
			if (dataformatProtocol.equalsIgnoreCase("UL2_0")){
				LOGGER.log(Level.INFO,"Instancing the agent IOTA " + dataformatProtocol);
				IOTA iota_ul = new IOTA_UL();
				idas = new Idas(iota_ul);
			} else if (dataformatProtocol.equalsIgnoreCase("JSON")) {
				LOGGER.log(Level.INFO,"Instancing the agent IOTA " + dataformatProtocol);
				IOTA iota_json = new IOTA_JSON();
				idas = new Idas(iota_json);
			} else if (dataformatProtocol.equalsIgnoreCase("SIGFOX")) {
				LOGGER.log(Level.INFO,"Instancing the agent IOTA " + dataformatProtocol);
				IOTA iota_sigfox = new IOTA_SIGFOX();
				idas = new Idas(iota_sigfox);
			} else if (dataformatProtocol.equalsIgnoreCase("LWM2M")) {
				LOGGER.log(Level.INFO,"Instancing the agent IOTA " + dataformatProtocol);
				IOTA iota_lwm2m = new IOTA_LWM2M();
				idas = new Idas(iota_lwm2m);
			} else if (dataformatProtocol.equalsIgnoreCase("CAYENNELPP") ||
					   dataformatProtocol.equalsIgnoreCase("CBOR") ||
					   dataformatProtocol.equalsIgnoreCase("LORAJSON") 
					) {
				LOGGER.log(Level.INFO,"Instancing the agent IOTA " + dataformatProtocol);
				IOTA iota_lora = new IOTA_LORA();
				idas = new Idas(iota_lora);	
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		LOGGER.log(Level.INFO, "getBaseUrl: " + idas.getBaseUrl()); 
		
		return idas;
	}
	

}
