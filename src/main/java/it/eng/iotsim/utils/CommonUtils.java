package it.eng.iotsim.utils;

import java.net.URL;

public abstract class CommonUtils {

	public static boolean isValidURL(String urlString){
	    boolean out = false;
		try{
	        URL url = new URL(urlString);
	        url.toURI();
	        out = true;
	    } 
	    catch (Exception exception){
	        out = false;
	    }
		
		return out;
	}
	
	
	/**
	 * @param fiwareservicepath
	 * @return
	 */
	public static String normalizeServicePath(String fiwareservicepath) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		return servicepath;
		
	}
	
	/**
	 * @param fiwareservicepath
	 * @return
	 */
	public static String cleanServicePath(String fiwareservicepath) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath.substring(1) : fiwareservicepath;
		return servicepath;
		
	}
	
	
}



