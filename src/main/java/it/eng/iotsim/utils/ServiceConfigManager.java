package it.eng.iotsim.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import it.eng.iotsim.tools.model.DeviceEntityBean;


public abstract class ServiceConfigManager {
	
	private static final Logger LOGGER = Logger.getLogger(ServiceConfigManager.class.getName() );
	

public static void main(String[] args) throws Exception {
				
	}

public static List<Double> getLatLongFromLocationDevice (DeviceEntityBean ocbDevice) {

	List<Double> output = new ArrayList<>();
	
	
	String geopoint_ser = ocbDevice.getLocation().getValue();
	String[] coords = geopoint_ser.split(",");
	
	try {
	
		output.add(Double.valueOf(coords[0]));
		output.add(Double.valueOf(coords[1]));
	}catch(Exception e) {
		
		e.printStackTrace();
		
		
	}
	return output;
}
	
}
