package it.eng.iotsim.tools;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

import com.google.gson.Gson;
import java.util.logging.*;
import com.sun.jersey.api.client.ClientResponse;

import it.eng.iotsim.configuration.ConfIdas;
import it.eng.iotsim.tools.base.DeviceManager;
import it.eng.iotsim.tools.model.IdasDevice;
import it.eng.iotsim.tools.model.IdasDeviceList;
import it.eng.iotsim.tools.model.IdasDeviceListGetResult;
import it.eng.iotsim.tools.model.IdasServiceList;
import it.eng.iotsim.utils.RestUtils;


public class Idas extends DeviceManager { 
	private static final Logger LOGGER = Logger.getLogger(Idas.class.getName() );
	
	private String path_services = ConfIdas.getString("iota.services"); 
	private String path_devices = ConfIdas.getString("iota.devices"); 
	
	/* Creates an instance of the IoT Agent */
	public Idas(IOTA iota) throws Exception{
		super(iota.getHost_protocol()+iota.getHost()+ ":"+iota.getNorthboundPort());
		LOGGER.log(Level.INFO, "Instance of IOTAgent: " + iota.getHost_protocol()+iota.getHost()+ ":"+iota.getNorthboundPort() );
	}
	
		
	
	
	@Override
	public JSONObject registerService(String fiwareservice, String fiwareservicepath, IdasServiceList services) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		JSONObject jResp;
		String url = this.getBaseUrl() + path_services;
		LOGGER.log(Level.INFO, "IDAS ENDPOINT: " + url);
		
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);
		LOGGER.log(Level.INFO, "IDAS HEADERS: " + headers);
		LOGGER.log(Level.INFO, "IDAS PAYLOAD: " + new Gson().toJson(services));
		String resp = RestUtils.consumePost(url, new Gson().toJson(services), headers);
		jResp = new JSONObject(resp);
		
		return jResp;
	}

	@Override
	public JSONObject registerDevice(String fiwareservice, String fiwareservicepath, IdasDeviceList devices) 
			throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		JSONObject jResp;
		String url = this.getBaseUrl() + path_devices;
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);
		
		String body = new Gson().toJson(devices);
		String resp = RestUtils.consumePost(url, body, headers);
		jResp = new JSONObject(resp);
		
		return jResp;
	}

	@Override
	public boolean deleteService(String fiwareservice, String fiwareservicepath, String resource, String apikey) 
			throws Exception {
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		LOGGER.log(Level.INFO, fiwareservice + " "+servicepath);
		
		String url = getBaseUrl() + path_services;
		
		Map<String, String> configHeaders = new HashMap<String, String>();
							configHeaders.put("fiware-service", fiwareservice);
							configHeaders.put("fiware-servicepath", servicepath);
		
		url += "?resource=" + resource
			+"&apikey=" + apikey;
		
		RestUtils.consumeDelete(url, configHeaders);
		
		return true;
	}

	@Override
	public boolean deleteDevice(String fiwareservice, String fiwareservicepath, String device_id) 
			throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		LOGGER.log(Level.INFO, fiwareservice + " " + servicepath + " " + device_id);
		
		String url = getBaseUrl() + path_devices;
		url += "/" + device_id;
		
		LOGGER.log(Level.INFO, "Invoking URL: " + url);
		
		Map<String, String> configHeaders = new HashMap<String, String>();
						configHeaders.put("fiware-service", fiwareservice);
						configHeaders.put("fiware-servicepath", servicepath);

		ClientResponse cr = RestUtils.consumeDelete(url, configHeaders);
		LOGGER.log(Level.INFO, "\t" + cr.getStatus());
		LOGGER.log(Level.INFO, "===================\n");
		
		return true;
	}
	
	public IdasDeviceListGetResult getDeviceList(String fiwareservice, String fiwareservicepath) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
		
		String url = getBaseUrl() + path_devices;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		
		String serviceResponse = RestUtils.consumeGet(url, headers);
		IdasDeviceListGetResult result = new Gson().fromJson(serviceResponse, IdasDeviceListGetResult.class);
		
		return result;
	}
	
	public IdasDevice getDevice(String fiwareservice, String fiwareservicepath, String deviceid) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;

		String url = getBaseUrl() + path_devices + "/" + deviceid;
		LOGGER.log(Level.INFO, "Invoking url: " + url);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		
		String serviceResponse = RestUtils.consumeGet(url, headers);
		IdasDevice result = new Gson().fromJson(serviceResponse, IdasDevice.class);
		
		return result;
	}
	
	@Override
	public boolean updateDevice (String fiwareservice, String fiwareservicepath, IdasDeviceList devices) throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;
	
		String url = this.getBaseUrl() + path_devices;
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);
		
		String body = new Gson().toJson(devices);
		boolean out = true;
		try {
		RestUtils.consumePut(url, body, MediaType.APPLICATION_JSON_TYPE, headers);
		}
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}
		return out;
	}

	
	
	
	public IdasServiceList getServices(String fiwareservice, String fiwareservicepath) 
			throws Exception{
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/" + fiwareservicepath;

		String url = getBaseUrl() + path_services;
		LOGGER.log(Level.INFO, url);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("fiware-service", fiwareservice);
		headers.put("fiware-servicepath", servicepath);
		
		String serviceResponse = RestUtils.consumeGet(url, headers);
		IdasServiceList result = new Gson().fromJson(serviceResponse, IdasServiceList.class);
				
		return result;
	}
	
	
}
