package it.eng.iotsim.tools.model;

public class ObjectAttribute {

	/*
	"lwm2mResourceMapping": {
				"Battery": {
					"objectType": 1111,
					"objectInstance": 0,
					"objectResource": 1
				},
              "status": {
					"objectType": 1111,
					"objectInstance": 0,
					"objectResource": 2
				}
			}
	*/
	//private Map<String, ObjectAttribute> map;
	
	private int objectType;
	private int objectInstance;
	private int objectResource;
	
	public ObjectAttribute(int objectType, int objectInstance, int objectResource) {
		this.objectType = objectType;
		this.objectInstance = objectInstance;
		this.objectResource = objectResource;
	}
	
	
	public int getObjectType() {
		return objectType;
	}

	public void setObjectType(int objectType) {
		this.objectType = objectType;
	}

	public int getObjectInstance() {
		return objectInstance;
	}

	public void setObjectInstance(int objectInstance) {
		this.objectInstance = objectInstance;
	}

	public int getObjectResource() {
		return objectResource;
	}

	public void setObjectResource(int objectResource) {
		this.objectResource = objectResource;
	}
	
}
