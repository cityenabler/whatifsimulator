package it.eng.iotsim.tools.model;

import java.util.HashSet;
import java.util.Set;

public class IdasDeviceGetResult extends IdasDevice{

	private String service;
	private String service_path;
	private Set<String> lazy;
	private String organization;
	private String device_owner;
	private String screenId;
	private String mobile_device;
	
	public IdasDeviceGetResult(String device_id, String entity_name,
			String entity_type, String transport, String dataformat_protocol, String endpoint, String service, String service_path,
			Set<String> lazy,  
			Set<IdasDeviceAttribute> attributes, Set<IdasDeviceStaticAttribute> static_attributes, 
			Set<IdasDeviceCommand> commands,
			Object internal_attributes,
			String organization, String device_owner, String screenId, String mobile_device) {
		
		super(device_id, entity_name, entity_type, transport, dataformat_protocol, endpoint, attributes, static_attributes, commands, internal_attributes);
		
		this.service = service;
		this.service_path = service_path;
		this.lazy = lazy;
		this.organization = organization;
		this.device_owner = device_owner;
		this.screenId = screenId;
		this.mobile_device = mobile_device;
	}
	
	public IdasDeviceGetResult(String device_id, String entity_name,
			String entity_type, String transport, String dataformat_protocol,String endpoint, String service, String service_path, 
			String organization, String device_owner, String screenId, String mobile_device){
		
		this(device_id, entity_name, entity_type, transport, dataformat_protocol, endpoint, service, service_path, //"HTTP"
				new HashSet<String>(),  
				new HashSet<IdasDeviceAttribute>(), 
				new HashSet<IdasDeviceStaticAttribute>(), 
				new HashSet<IdasDeviceCommand>(),
				new Object(),
				organization, device_owner, screenId, mobile_device);
		
	}
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getService_path() {
		return service_path;
	}
	public void setService_path(String service_path) {
		this.service_path = service_path;
	}
	
	public Set<String> getLazy() {
		return lazy;
	}
	public void setLazy(Set<String> lazy) {
		this.lazy = lazy;
	}
	
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public String getDevice_owner() {
		return device_owner;
	}
	public void setDevice_owner(String device_owner) {
		this.device_owner = device_owner;
	}
	
	public String getScreenId() {
		return screenId;
	}
	public void setScreenId(String screenId) {
		this.screenId = screenId;
	}
	
	public String getMobileDevice() {
		return mobile_device;
	}
	public void setMobileDevice(String mobile_device) {
		this.mobile_device = mobile_device;
	}
	
}
