/**
  {
"id": "anna1",
"type": "Device",
"TimeInstant": {
"type": "ISO8601",
"value": " ",
"metadata": {}
},
"device_owner": {
"type": "Text",
"value": "cedus-admin",
"metadata": {}
},
"location": {
"type": "geo:point",
"value": "-77.40245752036572,42.615192004180585",
"metadata": {}
},
"opType": {
"type": "Text",
"value": "created",
"metadata": {}
},
"organization": {
"type": "Text",
"value": "0888009313824014b81d94f9b6c1c4d3",
"metadata": {}
},
"screeanId": {
"type": "Text",
"value": "ANNA1",
"metadata": {}
},
"status": {
"type": "Text",
"value": " ",
"metadata": {}
}
}
*/

package it.eng.iotsim.tools.model;

import java.util.Date;

public class DeviceEntityBean extends CBEntity{
	
	private EntityAttribute<Date> dateModified;
	private EntityAttribute<String> device_owner; 
	private EntityAttribute<String> opType;
	private EntityAttribute<String> organization;
	private EntityAttribute<String> screenId; 
	private EntityAttribute<String> mobile_device;
	private EntityAttribute<String> location;
	
	private EntityAttribute<String> dataformat_protocol; 
	private EntityAttribute<String> transport_protocol;
	private EntityAttribute<String> retrieve_data_mode;
		
		
	public DeviceEntityBean() {
		this(new String("Device"));
	}
	
	public DeviceEntityBean(String type) {
		this(type, 
				new EntityAttribute<Date>(new Date()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()), 
				new EntityAttribute<String>(new String()), 
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String()),
				new EntityAttribute<String>(new String())
			);
	}
	
	public DeviceEntityBean(String type,
			
			EntityAttribute<Date> dateModified,
			EntityAttribute<String> device_owner,
			EntityAttribute<String> opType,
			EntityAttribute<String> organization,
			EntityAttribute<String> screenId,
			EntityAttribute<String> mobile_device,
			EntityAttribute<String> location,
			EntityAttribute<String> dataformat_protocol,
			EntityAttribute<String> transport_protocol,
			EntityAttribute<String> retrieve_data_mode) {
		
		super(new String(), type);
		this.dateModified = dateModified;
		this.device_owner = device_owner;
		this.opType = opType;
		this.organization = organization;
		this.screenId = screenId;
		this.mobile_device = mobile_device;
		this.location = location;
		this.dataformat_protocol = dataformat_protocol;
		this.transport_protocol = transport_protocol;
		this.retrieve_data_mode = retrieve_data_mode;

		String _id = screenId.getValue();
		this.setId(_id);
		
	}

	
	public EntityAttribute<Date> getDateModified() {
		return dateModified;
	}
	public void setDateModified(EntityAttribute<Date> dateModified) {
		this.dateModified = dateModified;
	}
	
	public EntityAttribute<String> getDeviceOwner() {
		return device_owner;
	}
	public void setResource(EntityAttribute<String> device_owner) {
		this.device_owner = device_owner;
	}
	
	public EntityAttribute<String> getOpType() {
		return opType;
	}
	public void setOpType(EntityAttribute<String> opType) {
		this.opType = opType;
	}
	
	public EntityAttribute<String> getOrganization() {
		return organization;
	}
	public void setOrganization(EntityAttribute<String> organization) {
		this.organization = organization;
	}
	
	public EntityAttribute<String> getScreenId() {
		return screenId;
	}
	public void setScreenId(EntityAttribute<String> screenId) {
		this.screenId = screenId;
	}
	
	public EntityAttribute<String> getMobileDevice() {
		return mobile_device;
	}
	public void setMobileDevice(EntityAttribute<String> mobile_device) {
		this.mobile_device = mobile_device;
	}
	
	public EntityAttribute<String> getLocation() {
		return location;
	}

	public void setLocation(EntityAttribute<String> location) {
		this.location = location;
	}

	public EntityAttribute<String> getDataformatProtocol() {
		return dataformat_protocol;
	}

	public void setDataformatProtocol(EntityAttribute<String> dataformat_protocol) {
		this.dataformat_protocol = dataformat_protocol;
	}
	
	public EntityAttribute<String> getTransportProtocol() {
		return transport_protocol;
	}

	public void setTransportProtocol(EntityAttribute<String> transport_protocol) {
		this.transport_protocol = transport_protocol;
	}
	
	public EntityAttribute<String> getRetrieveDataMode() {
		return retrieve_data_mode;
	}

	public void setRetrieveDataMode(EntityAttribute<String> retrieve_data_mode) {
		this.retrieve_data_mode = retrieve_data_mode;
	}
		
}
