package it.eng.iotsim.tools.model;

public class Scope {
	
	private String id;
	private String latitude;
	private String longitude;
	private String zoom;
	private String name;
	private String service;
	private String imageUrl;
	
	public Scope() {};
	
	public Scope(String id, String latitude, String longitude, String zoom, String name, String service, String imageUrl) {
		super();
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.zoom = zoom;
		this.name = name;
		this.service = service;
		this.imageUrl = imageUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getLat() {
		return latitude;
	}

	public void setLat(String latitude) {
		this.latitude = latitude;
	}

	public String getLng() {
		return longitude;
	}

	public void setLng(String longitude) {
		this.longitude = longitude;
	}

	public String getZoom() {
		return zoom;
	}

	public void setZoom(String zoom) {
		this.zoom = zoom;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Override
	public String toString() {
		return "[ id=" + id + ", latitude=" + latitude + ", longitude=" + longitude + ", zoom=" + zoom + ", name=" + name + ", service=" + service
				+ ", imageUrl=" + imageUrl + "]";
	}
}
