package it.eng.iotsim.tools.model;

import java.util.HashSet;
import java.util.Set;

public class IdasDeviceList {
	/*{
	*	  "devices": [
	*	    {
	*	      "device_id": "aaabb",
	*	      "entity_name": "aaabb",
	*	      "entity_type": "test8_air",
	*	      "attributes": [
	*	        {
	*	          "name": "pressure"
	*	        }
	*	      ],
	*	      "static_attributes": [
	*	        {
	*	          "name": "location",
	*	          "type": "geo:point",
	*	          "value": "11.11925319,46.07004214"
	*	        }
	*	      ]
	*	    }
	*	  ]
	*}
	*/
	
	private Set<IdasDevice> devices;
	
	public IdasDeviceList(){
		this.devices = new HashSet<IdasDevice>();
	}
	
	public IdasDeviceList(Set<IdasDevice> devices) {
		this.devices = devices;
	}

	public Set<IdasDevice> getDevices() {
		return devices;
	}

	public void setDevices(Set<IdasDevice> devices) {
		this.devices = devices;
	}
	
}
