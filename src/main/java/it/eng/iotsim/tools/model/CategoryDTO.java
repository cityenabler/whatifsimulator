package it.eng.iotsim.tools.model;

public class CategoryDTO {

	private String id;
	private String apikey; 
	private String name; 
	private EnablerForScope ref_enabler4scope;
	private String service_path;
	
	public CategoryDTO(
			String id,
			String apikey, 
			String name,
			EnablerForScope ref_enabler4scope, 
			String service_path) {
		this.id = id;
		this.apikey = apikey;
		this.name = name;
		this.ref_enabler4scope = ref_enabler4scope;
		this.service_path = service_path;
		
	}

	@Override
	public String toString() {
		return "[id=" + id + ", apikey=" + apikey + ", name=" + name + ", ref_enablerForScope=" + ref_enabler4scope + ", servicepath=" + service_path + "]";
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EnablerForScope getRef_enabler4scope() {
		return ref_enabler4scope;
	}

	public void setRef_enabler4scope(EnablerForScope ref_enabler4scope) {
		this.ref_enabler4scope = ref_enabler4scope;
	}

	public String getServicepath() {
		return service_path;
	}

	public void setServicepath(String service_path) {
		this.service_path = service_path;
	}


}
