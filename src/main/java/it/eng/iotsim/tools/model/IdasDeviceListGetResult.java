package it.eng.iotsim.tools.model;

import java.util.HashSet;
import java.util.Set;

public class IdasDeviceListGetResult {
	
	private Integer count;
	private Set<IdasDeviceGetResult> devices;
	
	public IdasDeviceListGetResult(){
		this.count = 0;
		this.devices = new HashSet<IdasDeviceGetResult>();
	}
	
	public IdasDeviceListGetResult(Set<IdasDeviceGetResult> devices) {
		this.devices = devices;
		this.count = this.devices.size();
	}
	
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Set<IdasDeviceGetResult> getDevices() {
		return devices;
	}
	public void setDevices(Set<IdasDeviceGetResult> devices) {
		this.devices = devices;
	}

}
