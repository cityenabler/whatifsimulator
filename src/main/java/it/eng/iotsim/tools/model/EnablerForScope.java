package it.eng.iotsim.tools.model;

public class EnablerForScope {
	
	
	private String id;
	
	private Scope ref_scope;
	
	private Enabler ref_enabler;
	
	public EnablerForScope() {
		super();
	}
	
	public EnablerForScope(String id, Scope ref_scope, Enabler ref_enabler) {
		super();
		this.id = id;
		this.ref_scope = ref_scope;
		this.ref_enabler = ref_enabler;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Scope getRefScope() {
		return ref_scope;
	}
	public void setRefScope(Scope ref_scope) {
		this.ref_scope = ref_scope;
	}
	public Enabler getRefEnabler() {
		return ref_enabler;
	}
	public void setRefEnabler(Enabler ref_enabler) {
		this.ref_enabler = ref_enabler;
	}

	@Override
	public String toString() {
		return "[id=" + id + ", ref_scope=" + ref_scope + ", ref_enabler=" + ref_enabler + "]";
	}
	
}

