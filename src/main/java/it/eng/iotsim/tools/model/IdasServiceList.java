package it.eng.iotsim.tools.model;

import java.util.Set;

public class IdasServiceList {
	/*{
	 * "services":[{
	 * 					"trust":"8970A9078A803H3BL98PINEQRW8342HBAMS",
	 * 					"cbHost":"http://192.168.150.15:1026",
	 * 					"entity_type":"test4_water",
	 * 					"apikey":"test4water",
	 * 					"resource":"/iot/d"
	 * 				}]
	 * }
	 */
	public class IdasService{
		
		private String trust;
		private String cbHost;
		private String entity_type;
		private String apikey;
		private String resource;
		
		public IdasService(String trust, String cbHost, String entity_type, String apikey, String resource) {
			this.trust = trust;
			this.cbHost = cbHost;
			this.entity_type = entity_type;
			this.apikey = apikey;
			this.resource = resource;
		}
		
		public String getTrust() {
			return trust;
		}
		public void setTrust(String trust) {
			this.trust = trust;
		}
		public String getCbHost() {
			return cbHost;
		}
		public void setCbHost(String cbHost) {
			this.cbHost = cbHost;
		}
		public String getEntity_type() {
			return entity_type;
		}
		public void setEntity_type(String entity_type) {
			this.entity_type = entity_type;
		}
		public String getApikey() {
			return apikey;
		}
		public void setApikey(String apikey) {
			this.apikey = apikey;
		}
		public String getResource() {
			return resource;
		}
		public void setResource(String resource) {
			this.resource = resource;
		}
		
	}

	
	private Set<IdasService> services;
	
	public IdasServiceList(Set<IdasService> services) {
		this.services = services;
	}

	public IdasServiceList() {
		// Empty Constructor. Does nothing
	}

	public Set<IdasService> getServices() {
		return services;
	}

	public void setServices(Set<IdasService> services) {
		this.services = services;
	}
	
}
