package it.eng.iotsim.tools;

import it.eng.iotsim.configuration.ConfIotaSigfox;

public class IOTA_SIGFOX extends IOTA {// extends Idas
	
	
	private String path_services; 
	private String path_devices; 
	private String host_protocol; 
	
	
	private String dataProtocol; 
	private String transportProtocol = "SIGFOX"; // HTTP, MQTT
	private String host;  
	private String northboundPort;  
	private String southboundPort;  
	
	private String baseurl;
	
	/**
	 * @param path_services
	 * @param path_devices
	 * @param host_protocol
	 * @param dataProtocol
	 * @param transportProtocol
	 * @param host
	 * @param northboundPort
	 * @param southboundPort
	 * @throws Exception
	 */
	public IOTA_SIGFOX() throws Exception {
		
		this.path_services = ConfIotaSigfox.getString("iota.services"); ;
		this.path_devices = ConfIotaSigfox.getString("iota.devices"); 
		this.host_protocol = ConfIotaSigfox.getString("iota.sigfox.host.protocol"); 
		this.dataProtocol = ConfIotaSigfox.getString("iota.sigfox.data.protocol");
		//this.transportProtocol = transportProtocol;
		this.host = ConfIotaSigfox.getString("iota.sigfox.host");
		this.northboundPort = ConfIotaSigfox.getString("iota.sigfox.northbound.port");
		this.southboundPort = ConfIotaSigfox.getString("iota.sigfox.southbound.port");
		this.baseurl = this.host_protocol + this.host + this.northboundPort;
		
	}
	
	
	public String getPath_services() {
		return path_services;
	}

	public void setPath_services(String path_services) {
		this.path_services = path_services;
	}

	public String getPath_devices() {
		return path_devices;
	}

	public void setPath_devices(String path_devices) {
		this.path_devices = path_devices;
	}

	public String getDataProtocol() {
		return dataProtocol;
	}

	public void setDataProtocol(String dataProtocol) {
		this.dataProtocol = dataProtocol;
	}

	public String getTransportProtocol() {
		return transportProtocol;
	}

	public void setTransportProtocol(String transportProtocol) {
		this.transportProtocol = transportProtocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getNorthboundPort() {
		return northboundPort;
	}

	public void setNorthboundPort(String northboundPort) {
		this.northboundPort = northboundPort;
	}

	public String getSouthboundPort() {
		return southboundPort;
	}

	public void setSouthboundPort(String southboundPort) {
		this.southboundPort = southboundPort;
	}

	public String getHost_protocol() {
		return host_protocol;
	}
	public void setHost_protocol(String host_protocol) {
		this.host_protocol = host_protocol;
	}
	
	public String getBaseUrl() {
		return baseurl;
	}
	public void setBaseUrl(String baseurl) {
		this.baseurl = baseurl;
	}
	
	


	
	
}
