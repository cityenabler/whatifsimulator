package it.eng.iotsim.configuration;

import java.util.MissingResourceException;
import java.util.Optional;
import java.util.ResourceBundle;

public class ConfIdas {
	private static final String BUNDLE_NAME = "configuration_idas"; //$NON-NLS-1$

	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
			.getBundle(BUNDLE_NAME);

	private ConfIdas() {
	}

	public static String getString(String key) {
		try {
			Optional<String> env = Optional.ofNullable(System.getenv(key));
			return env.orElse(RESOURCE_BUNDLE.getString(key));
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
