package it.eng.iotsim.servlet;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

import it.eng.iotsim.utils.IOTAgentManager;
import it.eng.iotsim.utils.ServiceConfigManager;
import it.eng.iotsim.configuration.ConfSimulator;
import it.eng.iotsim.manager.SimManager;
import it.eng.iotsim.manager.SimUtils;
import it.eng.iotsim.model.MeasureEntity;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.tools.model.IdasDevice;
import it.eng.iotsim.tools.model.IdasDeviceAttribute;
import it.eng.iotsim.tools.Idas;
import java.util.List;
import java.util.Set;
import java.util.logging.*;

@WebServlet(description = "Simulation start", urlPatterns = { "/start" })
public class StartHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629222L;

	private static final Logger LOGGER = Logger.getLogger(StartHandler.class.getName() );

	public StartHandler() {
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		JSONObject jResp = new JSONObject();
		jResp.put("error", false);

		boolean isRunning = false;
		
		
		try{
			String fiwareservice = request.getHeader("fiware-service");
			String fiwareservicepath = request.getHeader("fiware-servicepath");
			String deviceid = request.getHeader("deviceid");
			
			String categoryName = request.getParameter("categoryName");
			String categoryApikey = request.getParameter("categoryApikey");
			
			
			
			if (fiwareservicepath.substring(0,1).equalsIgnoreCase("/"))
				fiwareservicepath = fiwareservicepath.substring(1, fiwareservicepath.length());//fix if it has /
			
			// add configuration headers
			String measure_id = request.getHeader("measure_id");		// attribute: status
			String measure_type = request.getHeader("measure_type");	// attribute type: string
			String measure_range = request.getHeader("measure_range"); // range of value
			String measure_delay_seconds = request.getHeader("measure_delay_seconds"); // measure_delay_seconds
			
			String unit_Of_Measure = request.getHeader("unitOfMeasure"); 
			String radius_distance = request.getHeader("radius_distance"); 
			
			String mapcenterLat = request.getHeader("mapcenterLat"); 
			String mapcenterLng = request.getHeader("mapcenterLng");
			
			MeasureEntity simulatedMeasure = new MeasureEntity();
			simulatedMeasure.setId(measure_id);
			simulatedMeasure.setType(measure_type);
			simulatedMeasure.setValueRange(measure_range);
			
			String unitOfMeasure = ConfSimulator.getString("unit_of_measure");
			if (unit_Of_Measure != null) {
				unitOfMeasure = unit_Of_Measure;
			}
			simulatedMeasure.setUnitOfMeasure(unitOfMeasure);
			
			int radiusDistance = Integer.parseInt(ConfSimulator.getString("radius_distance"));
			if (radius_distance != null){
				radiusDistance = Integer.parseInt(radius_distance);
			}
			simulatedMeasure.setRadiusDistance(radiusDistance);
			
			if (mapcenterLat != null) {
				simulatedMeasure.setMapcenterLat(Double.valueOf(mapcenterLat));
				simulatedMeasure.setMapcenterLng(Double.valueOf(mapcenterLng));
			}
			
			
			int delaySeconds = Integer.parseInt(ConfSimulator.getString("period_inseconds"));
			if (measure_delay_seconds != null){
				delaySeconds = Integer.parseInt(measure_delay_seconds);
			}
			simulatedMeasure.setDelaySeconds(delaySeconds);
			
			LOGGER.log(Level.INFO,"CONTEXT service:" + fiwareservice + "|servicepath:" + fiwareservicepath);
			LOGGER.log(Level.INFO,"DEVICE MEASURE CONFIG measure_id:" + measure_id + "|measure_type:" + measure_type+ "|measure_range:" + measure_range);

			// STARTS THE DEVICE SIMULATION ****************************************
			if (SimUtils.hasMeasureId(measure_id)){
				isRunning = SimManager.isRunningTimeTask(fiwareservice, fiwareservicepath, deviceid, measure_id);
				if(!isRunning){
					LOGGER.log(Level.INFO,"TimeTask " + fiwareservice+"|"+fiwareservicepath+"|"+deviceid+"|" + measure_id + " STARTING... ");
					SimManager.startTimeTask(fiwareservice, fiwareservicepath, categoryName , categoryApikey, deviceid, simulatedMeasure);				
					LOGGER.log(Level.INFO,"STARTED.");
					isRunning = true;
				}
			// STARTS THE CONTEXT SIMULATION ****************************************
			} else {
				
				
				
				//Orion device
				DeviceEntityBean ocbDevice = SimUtils.getOCBDevice(fiwareservice, fiwareservicepath, deviceid);
				
				
					
					// get IDAS device to read all the attribute *************************
					String dataformatProtocol = ocbDevice.getDataformatProtocol().getValue();
					Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
					deviceid = ocbDevice.getId();
					IdasDevice idasdev = idas.getDevice(fiwareservice, fiwareservicepath, deviceid);
					Set<IdasDeviceAttribute> idasattrs = idasdev.getAttributes();
					
					
					List<Double> location = ServiceConfigManager.getLatLongFromLocationDevice(ocbDevice);
					
					if (location.get(0) == 0 && location.get(1) == 0) {
						
						location.set(0, Double.valueOf(mapcenterLat)) ;
						location.set(1, Double.valueOf(mapcenterLng)) ;
						
					}
						
					
					
					//all attributes for single device
					for (IdasDeviceAttribute idasattr : idasattrs) {
						String attributeid = idasattr.getObject_id();
						String attributetype = idasattr.getType();
						simulatedMeasure = new MeasureEntity();
						simulatedMeasure.setId(attributeid);
						simulatedMeasure.setType(attributetype);
						simulatedMeasure.setMapcenterLat(location.get(0));
						simulatedMeasure.setMapcenterLng(location.get(1));
						simulatedMeasure.setRadiusDistance(radiusDistance);
						simulatedMeasure.setUnitOfMeasure(unitOfMeasure);
						
						isRunning = SimManager.isRunningTimeTask(fiwareservice, fiwareservicepath, ocbDevice.getId(), attributeid);
						if(!isRunning){
							
							// Get the configurations from file properties in order to get the value range configured
							MeasureEntity defaultMeasure = SimUtils.loadBusinessDataModelD(categoryName,simulatedMeasure );
							
							if (defaultMeasure != null) {
									
								simulatedMeasure.setValueRange(defaultMeasure.getValueRange());
								simulatedMeasure.setDelaySeconds(delaySeconds);
								
								// Then start simulation	
								LOGGER.log(Level.INFO,"TimeTask " + fiwareservice+"|"+fiwareservicepath+"|"+deviceid+"|"+attributeid + " STARTING... ");
								SimManager.startTimeTask(fiwareservice, fiwareservicepath, categoryName, categoryApikey, deviceid, simulatedMeasure);	
								LOGGER.log(Level.INFO,"STARTED.");
								isRunning = true;
							} 
						} 
					}
				
			}

			JSONObject jresp = new JSONObject();
			jresp.put("isRunning", isRunning);
			response.getWriter().write(jresp.toString());
		}
		catch (Exception e) {
			LOGGER.log(Level.INFO,e.getMessage());
			response.setStatus(500);
			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString()); 
			jResp.put("isRunning", isRunning); 
			response.getWriter().write(jResp.toString());
		}
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}





}

