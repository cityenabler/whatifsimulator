package it.eng.iotsim.servlet;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

import it.eng.iotsim.manager.SimManager;
import it.eng.iotsim.manager.SimUtils;
import it.eng.iotsim.tools.Idas;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.tools.model.IdasDevice;
import it.eng.iotsim.tools.model.IdasDeviceAttribute;
import it.eng.iotsim.utils.IOTAgentManager;

import java.util.Set;
import java.util.logging.*;

@WebServlet(description = "Simulation stop", urlPatterns = { "/stop" })
public class StopHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629222L;

	private static final Logger LOGGER = Logger.getLogger(StopHandler.class.getName() );

	public StopHandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		JSONObject jResp = new JSONObject();
		jResp.put("error", false);

		boolean isRunning = false;

		try{
			String fiwareservice = request.getHeader("fiware-service");
			String fiwareservicepath = request.getHeader("fiware-servicepath");
			String deviceid = "";
			String attributeid = ""; //device attribute used for simulation
			
			if(request.getHeader("deviceid")!= null && request.getHeader("deviceid")!= "") {
				deviceid = request.getHeader("deviceid");
			}
			
			if(request.getHeader("measure_id")!=null && request.getHeader("measure_id")!="") {
				attributeid = request.getHeader("measure_id");
			}
			
			if (fiwareservicepath.substring(0,1).equalsIgnoreCase("/"))
				fiwareservicepath = fiwareservicepath.substring(1, fiwareservicepath.length());//fix if it has /
			
			if(deviceid!=null && deviceid!="") {
				
			

				if (SimUtils.hasDeviceId(deviceid)){
					
					
					if (attributeid!=null && attributeid!="" && SimUtils.hasDeviceId(attributeid)) {//if we have the device, but have not attribute, so I stop all attributes
					
						
						isRunning = SimManager.isRunningTimeTask(fiwareservice, fiwareservicepath, deviceid, attributeid);
						if(isRunning){
							SimManager.stopTimeTask(fiwareservice, fiwareservicepath, deviceid, attributeid);
							LOGGER.log(Level.INFO,"TimeTask " + fiwareservice+"|"+fiwareservicepath+"|"+deviceid+"|"+attributeid + " STOPPED.");
							isRunning = false;
						}
					}else {
						
						Set<DeviceEntityBean> devices = SimUtils.getContextAllDevices(fiwareservice, fiwareservicepath);
						
						for (DeviceEntityBean dev : devices) {
							String deviceidbean = dev.getId();
							
							if (deviceidbean.equalsIgnoreCase(deviceid)) {
								
								stopSingleDevice (dev, fiwareservice, fiwareservicepath);
								isRunning = false;
								
							}
						}
					}
					
					
				} else if (!SimUtils.hasDeviceId(deviceid)) {//stop all
							
							Set<DeviceEntityBean> devices = SimUtils.getContextAllDevices(fiwareservice, fiwareservicepath);
							
							if(devices!=null) {
								for (DeviceEntityBean dev : devices) {
									stopSingleDevice (dev, fiwareservice, fiwareservicepath);
								}
								isRunning = false;
							}
							
				}
			}else {
				isRunning = SimManager.isRunning(fiwareservice, fiwareservicepath);
				if(isRunning) {
					SimManager.stopTimeTask(fiwareservice, fiwareservicepath);
					LOGGER.log(Level.INFO,"TimeTask " + fiwareservice+"|"+fiwareservicepath + " STOPPED.");
					isRunning = false;
				}
			}

			jResp.put("isRunning", isRunning);
			jResp.put("message", "stopped"); 
			response.getWriter().write(jResp.toString());
		}
		catch (Exception e) {
			LOGGER.log(Level.INFO,e.getMessage());
			response.setStatus(500);

			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString()); 
			jResp.put("isRunning", isRunning); 
			response.getWriter().write(jResp.toString());
		}
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}


	private void stopSingleDevice (DeviceEntityBean dev, String fiwareservice, String fiwareservicepath) {
		
		String deviceid = dev.getId();
		// get IDAS device to read all the attribute *************************
		String dataformatProtocol = dev.getDataformatProtocol().getValue();
		Idas idas;
		IdasDevice idasdev = null;
		try {
			idas = IOTAgentManager.switchIOTA(dataformatProtocol);
			idasdev = idas.getDevice(fiwareservice, fiwareservicepath, deviceid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Set<IdasDeviceAttribute> idasattrs = idasdev.getAttributes();
		for (IdasDeviceAttribute idasattr : idasattrs) {
			String attributeid = idasattr.getObject_id();
			
			boolean isRunning = SimManager.isRunningTimeTask(fiwareservice, fiwareservicepath, deviceid, attributeid);
			
			if(isRunning){
				SimManager.stopTimeTask(fiwareservice, fiwareservicepath, deviceid, attributeid);
				LOGGER.log(Level.INFO,"TimeTask " + fiwareservice+"|"+fiwareservicepath+"|"+deviceid+"|"+attributeid + " STOPPED.");
			}
		}
	}
	
}

