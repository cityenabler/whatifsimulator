package it.eng.iotsim.servlet;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.json.JSONObject;
import com.google.gson.Gson;

import it.eng.iotsim.configuration.ConfSimulator;

import it.eng.iotsim.manager.SimUtils;
import it.eng.iotsim.model.MeasureEntity;

@WebServlet(description = "Simulation configuration", urlPatterns = { "/config" })
public class ConfigurationHandler extends HttpServlet{
	private static final long serialVersionUID = -8956961196989629222L;

	private static final Logger LOGGER = Logger.getLogger(ConfigurationHandler.class.getName() );

	public ConfigurationHandler() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		JSONObject jResp = new JSONObject();
		jResp.put("error", false);
		String res = "";
		
		try {
			String service = request.getHeader("fiware-service"); 
			String servicepath = request.getHeader("fiware-servicepath"); 
			String measure_id = request.getHeader("measure_id"); 
			String measure_type = request.getHeader("measure_type"); 
			
			String urbanserviceName = request.getParameter("categoryName");
			
			
			MeasureEntity mesResp = new MeasureEntity();
			Set<MeasureEntity> measures = new HashSet<>();
			
			if (servicepath.substring(0,1).equalsIgnoreCase("/"))
				servicepath = servicepath.substring(1, servicepath.length());
			
			int measure_delay_seconds = Integer.parseInt(ConfSimulator.getString("period_inseconds"));
			
		  
			measures = SimUtils.loadBusinessDataModel(urbanserviceName, measure_id, measure_type, service);
			
			for (MeasureEntity mes:measures){
				if (mes.getId().equalsIgnoreCase(measure_id)){
					mesResp = mes;
				}
			}
			
			mesResp.setDelaySeconds(measure_delay_seconds);
			
			res = new Gson().toJson(mesResp);
			response.getWriter().write(res);
			
		}
		catch (Exception e) {
			LOGGER.log(Level.INFO,e.getMessage());
			response.setStatus(500);

			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString());
			response.getWriter().write(jResp.toString());
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}

