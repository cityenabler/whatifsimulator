package it.eng.iotsim.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import it.eng.iotsim.manager.SimManager;
import it.eng.iotsim.manager.SimUtils;
import it.eng.iotsim.tools.Idas;
import it.eng.iotsim.tools.model.DeviceEntityBean;
import it.eng.iotsim.tools.model.IdasDevice;
import it.eng.iotsim.tools.model.IdasDeviceAttribute;
import it.eng.iotsim.utils.IOTAgentManager;

import java.util.Set;
import java.util.logging.*;


@WebServlet(description = "Simulation status", urlPatterns = { "/status" })
public class StatusHandler extends HttpServlet {
	private static final long serialVersionUID = -8956961196989629222L;

	private static final Logger LOGGER = Logger.getLogger(StatusHandler.class.getName() );

	public StatusHandler() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		JSONObject jResp = new JSONObject();
		jResp.put("error", false);

		
		boolean isRunning = true;

		try {		
			String fiwareservice = request.getHeader("fiware-service");
			String fiwareservicepath = request.getHeader("fiware-servicepath");
			String deviceid = request.getHeader("deviceid");
			String attributeid = request.getHeader("measure_id"); //device attribute used for simulation
			
			if (fiwareservicepath.substring(0,1).equalsIgnoreCase("/"))
				fiwareservicepath = fiwareservicepath.substring(1, fiwareservicepath.length());//fix if it has /
						
			if (SimUtils.hasMeasureId(attributeid)) {
				isRunning = SimManager.isRunningTimeTask(fiwareservice, fiwareservicepath, deviceid, attributeid);
				LOGGER.log(Level.INFO,"TimeTask device " + deviceid+"|"+attributeid + " is running: " + isRunning);
			} else {
					
					DeviceEntityBean dev = SimUtils.getOCBDevice(fiwareservice, fiwareservicepath, deviceid);
				
					// get IDAS device to read all the attribute *************************
					String dataformatProtocol = dev.getDataformatProtocol().getValue();
					Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
					deviceid = dev.getId();

				try {
					IdasDevice idasdev = idas.getDevice(fiwareservice, fiwareservicepath, deviceid);
					Set<IdasDeviceAttribute> idasattrs = idasdev.getAttributes();
					for (IdasDeviceAttribute idasattr : idasattrs) {
						attributeid = idasattr.getObject_id();
						isRunning = SimManager.isRunningTimeTask(fiwareservice, fiwareservicepath, deviceid, attributeid);
						if(isRunning){
							LOGGER.log(Level.INFO,"TimeTask device " + deviceid+"|"+attributeid + " is running: " + isRunning);
							break;
						}
					}
				}catch(Exception e){
					LOGGER.log(Level.INFO,e.getMessage());
					
				}
			}
			
			JSONObject jresp = new JSONObject();
			jresp.put("isRunning", isRunning);
			response.getWriter().write(jresp.toString());
		}
		catch (Exception e) {
			LOGGER.log(Level.INFO,e.getMessage());
			response.setStatus(500);

			jResp = new JSONObject();
			jResp.put("error", true); 
			jResp.put("message", e.toString());
			jResp.put("isRunning", false); // in case of error is not running

			response.getWriter().write(jResp.toString());
		}
	}


	/*check the simulation about the device on the body input */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
		
		
		String body = request.getReader().lines().reduce("", (accumulator, actual) -> accumulator + actual);
		
		JSONArray devicesJArr = new JSONArray(body);
		JSONArray responseJArr = new JSONArray();
		
		for (int i = 0; i < devicesJArr.length(); i++) {
            JSONObject device = devicesJArr.getJSONObject(i);
            
            String fiwareservice = device.getString("fiwareservice");
            String fiwareservicepath = device.getString("fiwareservicepath");
            String deviceId = device.getString("deviceId");
            String dataformatProtocol = device.getString("dataformatProtocol");
        	
        	try {
        		
        		Idas idas = IOTAgentManager.switchIOTA(dataformatProtocol);
        		boolean isRunning = false;
        		IdasDevice idasdev = idas.getDevice(fiwareservice, fiwareservicepath, deviceId);
        		Set<IdasDeviceAttribute> idasattrs = idasdev.getAttributes();
        		for (IdasDeviceAttribute idasattr : idasattrs) {
        			String attributeid = idasattr.getObject_id();
        			isRunning = SimManager.isRunningTimeTask(fiwareservice, fiwareservicepath, deviceId, attributeid);
        			if(isRunning){
        				LOGGER.log(Level.INFO,"TimeTask device " + deviceId+"|"+attributeid + " is running: " + isRunning);
        				break;
        			}
        		}
        		device.put("isRunningSimulation", isRunning);
        		
        	}catch(Exception e){
        		LOGGER.log(Level.SEVERE,e.getMessage());
        		
        	}
            
            responseJArr.put(device);
            
        }
		response.getWriter().write(responseJArr.toString());
	}

}

