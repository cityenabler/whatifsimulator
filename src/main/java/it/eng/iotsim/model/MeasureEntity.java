package it.eng.iotsim.model;

public class MeasureEntity {
		
	private String id;
	private String type;
	private String valueRange;
	private Object value;
	private int delaySeconds;
	private String unitOfMeasure;
	private int radiusDistance;
	private double mapcenterLat;
	private double mapcenterLng;
	
	
	
	
	public MeasureEntity( String id, String type, String valueRange, Object value, int delaySeconds) {
		this.id = id;
		this.type = type;
		this.valueRange = valueRange;
		this.value = value;
		this.delaySeconds = delaySeconds;
	}

	public MeasureEntity(){
		this.id = "";
		this.type = "";
		this.valueRange = "";
		this.value = "";
		this.delaySeconds = 60;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getValueRange() {
		return valueRange;
	}
	public void setValueRange(String valueRange) {
		this.valueRange = valueRange;
	}
	
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	
	public int getDelaySeconds() {
		return delaySeconds;
	}
	public void setDelaySeconds(int delaySeconds) {
		this.delaySeconds = delaySeconds;
	}

	public String getUnitOfMeasure() {
		return unitOfMeasure;
	}

	public void setUnitOfMeasure(String unitOfMeasure) {
		this.unitOfMeasure = unitOfMeasure;
	}

	public int getRadiusDistance() {
		return radiusDistance;
	}

	public void setRadiusDistance(int radiusDistance) {
		this.radiusDistance = radiusDistance;
	}

	public double getMapcenterLat() {
		return mapcenterLat;
	}

	public void setMapcenterLat(double mapcenterLat) {
		this.mapcenterLat = mapcenterLat;
	}

	public double getMapcenterLng() {
		return mapcenterLng;
	}

	public void setMapcenterLng(double mapcenterLng) {
		this.mapcenterLng = mapcenterLng;
	}



}
