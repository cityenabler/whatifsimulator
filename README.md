whatifsimulator
======
Misures simulator for IotDevice
 

## Requirements  
| Framework                                                                        |Version  |Licence             |Note 																	|
|--------------------------------------------------------------------------------------------|---------|--------------------|-------------------------------------------------------------------------|

TO DO


## Libraries

This project is based on the following software libraries and frameworks.

|Framework                           |Version       |Licence                                      |
|------------------------------------|--------------|---------------------------------------------|

TO DO

## How do I get set up? 
TO DO

Use the [iotsim.env](iotsim.env) file.
Only for locale development refer to [iotsim_locale.env](iotsim_locale.env) file.

## Support / Contact / Contribution

-   [*Filippo.Giuffrida@eng.it*](mailto:Filippo.Giuffrida@eng.it)
-   [*Antonino.Sirchia@eng.it*](mailto:Antonino.Sirchia@eng.it)

## Copying and License

This code is licensed under [Apache License 2.0] (http://www.apache.org/licenses/LICENSE-2.0)

